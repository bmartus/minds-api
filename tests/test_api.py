import pytest
import requests_mock

from minds import Minds
from minds.endpoints import CHANNEL_URLF
from requests.utils import cookiejar_from_dict

VALID_GUID = '123456789012345678'


@pytest.fixture()
def mock_req():
    with requests_mock.Mocker() as m:
        m.get('https://www.minds.com/login', cookies=cookiejar_from_dict({'XSRF-TOKEN': 'test_xsrf_token'}))
        yield m


def test_is_guid():
    assert Minds.is_guid('12345') is False
    assert Minds.is_guid(VALID_GUID) is True


def test_guid_from_url(mock_req):
    mock_req.get(CHANNEL_URLF('test_channel'), json={'channel': {'guid': VALID_GUID}})
    m = Minds(login=False)
    # from url
    assert m.get_guid('https://www.minds.com/newsfeed/123456789012345678') == VALID_GUID
    # from guid
    assert m.get_guid(VALID_GUID) == VALID_GUID
    # from channel name
    assert m.get_guid('test_channel') == VALID_GUID
    # from channel_name in url
    assert m.get_guid('https://www.minds.com/test_channel') == VALID_GUID


def test_comments(mock_req):
    m = Minds(login=False)
    resp_data = [{'comment': 'foo'}, {'comment': 'bar'}]
    mock_req.get('https://www.minds.com/api/v1/comments/123456789012345678?limit=12',
                 json=resp_data)
    result = m.comments(guid=VALID_GUID)
    assert result == resp_data

