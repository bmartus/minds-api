#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'requests',
    'toml',
]

setup_requirements = [
    'pytest-runner',
]

test_requirements = [
    'pytest',
    'requests_mock',
]

setup(
    name='minds',
    version='0.1.2',
    description="unofficial api for minds.com",
    long_description=readme + '\n\n' + history,
    author="Bernardas Ališauskas",
    author_email='bernardas.alisauskas@pm.me',
    url='https://gitlab.com/granitosaurus/minds-api',
    packages=find_packages(include=['minds', 'minds.sections']),
    include_package_data=True,
    install_requires=requirements,
    license="GNU General Public License v3",
    zip_safe=False,
    keywords='minds',
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    test_suite='tests',
    tests_require=test_requirements,
    setup_requires=setup_requirements,
)
